## ---------------------------------------------------------------------------------------------
## -- Theory

# A MDP is a 4-tuple (S,A,P.(.,.),R.(.,.)))
# S: finite set of states
# A: finite set of actions
# P[a](s,s')=Pr(s[t+1]=s'|s[t]=s,a[t]=a)
# R[a](s,s'): immediate (or expected immediate) reward

# Policy:
# PI: function that specifies the action PI[s] that the
#     decision maker should choose when in state s

# Optimal solution: maximum total expected (discounted) reward
#   MAX{
#        SUM[t=0,N]{
#            (discount^t)*R[a](s[t],s[t+1])|a[t]=PI[s[t]]
#        }
#    }
#

## ---------------------------------------------------------------------------------------------
## -- Utils

def initialize(obj,**kwargs):
    import itertools
    for k,v in kwargs.iteritems():
        obj.__dict__[k] = v

## ---------------------------------------------------------------------------------------------
## -- MDP data structure

class MDP(object):
    def __init__(self, **kwargs):
        initialize(self, **kwargs)
    
    def P(self,a,s,s_):
        """
        transition probability of going from state s
        to state s' by taking action a        
        """
        tps =  self.transition_probabilities[(s,a)]
        try:
            return tps[s_]
        except KeyError:
            return 0
    
    def R(self,a,s,s_):
        try:
            return self.rewards[(s,a,s_)]
        except KeyError:
            return 0
    
    def A(self,s):
        actions = {}
        for action in self.actions:
            try:
                tp = self.transition_probabilities[(s,action)]
                actions[action] = None
            except KeyError:
                pass
        return actions
    
    def RN(self,s):
        return self.terminal_rewards[s]

## ---------------------------------------------------------------------------------------------
## -- Value Iteration

# Input: model, number of epochs, disounc factor (gamma)
# Output: function PI:{s,t} -> a 


def value_iteration(epochs,model,discount_factor):
    gamma = discount_factor
    rewards = {}
    policy = {}

    # terminal rewards
    print "calculating terminal rewards ..."
    for state in model.states:
        print "    calculating terminal reward for state: '%s' ..." % state
        r = model.RN(state)
        print "      -> terminal reward = %s" % r
        rewards[(epochs, state)] = r
    print "terminal rewards calculated!"
    print "  -> rewards: %s" % rewards
    print ""

    # backward induction
    for t in reversed(xrange(1, epochs)):
        print " ### Epoch %s ###" % t
        for state in model.states:
            print "    calculating expected rewards for state: '%s' ..." % state
            _actions = model.A(state)
            print "possible actions: %s" % _actions
            possible_rewards = {}
            for action in _actions:
                print "        using action: '%s'" % action
                # note: this step is different from the algorithm
                #       on page 92 of Puterman, because the MDP
                #       here accepts different rewards for the same
                #       action depending on the start-/end-state
                r = 0
                for state_ in model.states:
                    r += model.R(action,state,state_) + (gamma**t)*model.P(action,state,state_)*rewards[(t+1,state_)]
                print "          -> reward: %s" % r
                possible_rewards[action] = r
            
            if len(possible_rewards) == 0:
                print "STUCK! No action to exit state '%s'" % state
            else:
                max_action = max(possible_rewards,key=possible_rewards.get)
                max_reward = possible_rewards[max_action]
                print "    best action is: '%s', reward is: '%s'" % (max_action, max_reward)
                rewards[(t,state)] = max_reward
                policy[(t,state)] = max_action
    
    return {"policy": policy, "expected rewards": rewards}

## ---------------------------------------------------------------------------------------------
## -- Sample data: random

s_states = {"state 0": None
           ,"state 1": None
           ,"state 2": None}
s_actions = {"action 0": None
            ,"action 1": None}
s_transition_probabilities = {("state 0", "action 0"): {"state 0": 0.50
                                                       ,"state 2": 0.50}
                             ,("state 0", "action 1"): {"state 2": 1.00}
                             ,("state 1", "action 0"): {"state 0": 0.70
                                                       ,"state 1": 0.10
                                                       ,"state 2": 0.20}
                             ,("state 1", "action 1"): {"state 1": 0.95
                                                       ,"state 2": 0.05}
                             ,("state 2", "action 0"): {"state 0": 0.40
                                                       ,"state 2": 0.60}
                             ,("state 2", "action 1"): {"state 0": 0.30
                                                       ,"state 1": 0.30
                                                       ,"state 2": 0.40}}
# s_rewards = {("state 1", "action 0", "state 0"): +5.00
#             ,("state 2", "action 1", "state 0"): -1.00}
s_rewards = {("state 1", "action 0", "state 0"): +5.00
            ,("state 2", "action 1", "state 0"): -1.00}
s_terminal_rewards = {"state 0": 0
                     ,"state 1": 0
                     ,"state 2": 10000}

mdp = MDP(states=s_states
         ,actions=s_actions
         ,transition_probabilities=s_transition_probabilities
         ,rewards=s_rewards
         ,terminal_rewards=s_terminal_rewards)

## ---------------------------------------------------------------------------------------------
## -- Sample data: inventory model

inventory_states = {
     "inventory level: 0": None
    ,"inventory level: 1": None
    ,"inventory level: 2": None
    ,"inventory level: 3": None
}

inventory_actions = {
     "order 0 item(s)": None
    ,"order 1 item(s)": None
    ,"order 2 item(s)": None
    ,"order 3 item(s)": None
}

inventory_transition_probabilities = {
    ("inventory level: 0", "order 0 item(s)"): {
            "inventory level: 0": 1.00
           # ,"inventory level: 1": 0.00
           # ,"inventory level: 2": 0.00
           # ,"inventory level: 3": 0.00
           }
   ,("inventory level: 0", "order 1 item(s)"): {
            "inventory level: 0": 0.25
           ,"inventory level: 1": 0.25
           # ,"inventory level: 2": 0.00
           # ,"inventory level: 3": 0.00
           }
   ,("inventory level: 0", "order 2 item(s)"): {
            "inventory level: 0": 0.25
           ,"inventory level: 1": 0.50
           ,"inventory level: 2": 0.25
           # ,"inventory level: 3": 0.00
           }
   ,("inventory level: 0", "order 3 item(s)"): {
            # "inventory level: 0": 0.00
           "inventory level: 1": 0.25
           ,"inventory level: 2": 0.50
           ,"inventory level: 3": 0.25
           }


   ,("inventory level: 1", "order 0 item(s)"): {
            "inventory level: 0": 0.75
           ,"inventory level: 1": 0.25
           # ,"inventory level: 2": 0.00
           # ,"inventory level: 3": 0.00
           }
   ,("inventory level: 1", "order 1 item(s)"): {
            "inventory level: 0": 0.25
           ,"inventory level: 1": 0.50
           ,"inventory level: 2": 0.25
           # ,"inventory level: 3": 0.00
           }
   ,("inventory level: 1", "order 2 item(s)"): {
            # "inventory level: 0": 0.00
           "inventory level: 1": 0.25
           ,"inventory level: 2": 0.50
           ,"inventory level: 3": 0.25
           }
   # ,("inventory level: 1", "order 3 item(s)"): {
   #          "inventory level: 0": 0
   #         ,"inventory level: 1": 0
   #         ,"inventory level: 2": 0
   #         ,"inventory level: 3": 0
   #         }


   ,("inventory level: 2", "order 0 item(s)"): {
            "inventory level: 0": 0.25
           ,"inventory level: 1": 0.50
           ,"inventory level: 2": 0.25
           # ,"inventory level: 3": 0.00
           }
   ,("inventory level: 2", "order 1 item(s)"): {
            # "inventory level: 0": 0.00
           "inventory level: 1": 0.25
           ,"inventory level: 2": 0.50
           ,"inventory level: 3": 0.25
           }
   # ,("inventory level: 2", "order 2 item(s)"): {
   #          "inventory level: 0": 0
   #         ,"inventory level: 1": 0
   #         ,"inventory level: 2": 0
   #         ,"inventory level: 3": 0
   #         }
   # ,("inventory level: 2", "order 3 item(s)"): {
   #          "inventory level: 0": 0
   #         ,"inventory level: 1": 0
   #         ,"inventory level: 2": 0
   #         ,"inventory level: 3": 0
   #         }


   ,("inventory level: 3", "order 0 item(s)"): {
            # "inventory level: 0": 0.00
           "inventory level: 1": 0.25
           ,"inventory level: 2": 0.50
           ,"inventory level: 3": 0.25
           }
   # ,("inventory level: 3", "order 1 item(s)"): {
   #          "inventory level: 0": 0
   #         ,"inventory level: 1": 0
   #         ,"inventory level: 2": 0
   #         ,"inventory level: 3": 0
   #         }
   # ,("inventory level: 3", "order 2 item(s)"): {
   #          "inventory level: 0": 0
   #         ,"inventory level: 1": 0
   #         ,"inventory level: 2": 0
   #         ,"inventory level: 3": 0
   #         }
   # ,("inventory level: 3", "order 3 item(s)"): {
   #          "inventory level: 0": 0
   #         ,"inventory level: 1": 0
   #         ,"inventory level: 2": 0
   #         ,"inventory level: 3": 0
   #         }
}

inventory_rewards = {
    ("inventory level: 0", "order 0 item(s)", "inventory level: 0"): +0.00
   ,("inventory level: 0", "order 0 item(s)", "inventory level: 1"): +0.00
   ,("inventory level: 0", "order 0 item(s)", "inventory level: 2"): +0.00
   ,("inventory level: 0", "order 0 item(s)", "inventory level: 3"): +0.00

   ,("inventory level: 0", "order 1 item(s)", "inventory level: 0"): -1.00
   ,("inventory level: 0", "order 1 item(s)", "inventory level: 1"): -1.00
   ,("inventory level: 0", "order 1 item(s)", "inventory level: 2"): -1.00
   ,("inventory level: 0", "order 1 item(s)", "inventory level: 3"): -1.00

   ,("inventory level: 0", "order 2 item(s)", "inventory level: 0"): -2.00
   ,("inventory level: 0", "order 2 item(s)", "inventory level: 1"): -2.00
   ,("inventory level: 0", "order 2 item(s)", "inventory level: 2"): -2.00
   ,("inventory level: 0", "order 2 item(s)", "inventory level: 3"): -2.00

   ,("inventory level: 0", "order 3 item(s)", "inventory level: 0"): -5.00
   ,("inventory level: 0", "order 3 item(s)", "inventory level: 1"): -5.00
   ,("inventory level: 0", "order 3 item(s)", "inventory level: 2"): -5.00
   ,("inventory level: 0", "order 3 item(s)", "inventory level: 3"): -5.00



   ,("inventory level: 1", "order 0 item(s)", "inventory level: 0"): +5.00
   ,("inventory level: 1", "order 0 item(s)", "inventory level: 1"): +5.00
   ,("inventory level: 1", "order 0 item(s)", "inventory level: 2"): +5.00
   ,("inventory level: 1", "order 0 item(s)", "inventory level: 3"): +5.00

   ,("inventory level: 1", "order 1 item(s)", "inventory level: 0"): +0.00
   ,("inventory level: 1", "order 1 item(s)", "inventory level: 1"): +0.00
   ,("inventory level: 1", "order 1 item(s)", "inventory level: 2"): +0.00
   ,("inventory level: 1", "order 1 item(s)", "inventory level: 3"): +0.00

   ,("inventory level: 1", "order 2 item(s)", "inventory level: 0"): -3.00
   ,("inventory level: 1", "order 2 item(s)", "inventory level: 1"): -3.00
   ,("inventory level: 1", "order 2 item(s)", "inventory level: 2"): -3.00
   ,("inventory level: 1", "order 2 item(s)", "inventory level: 3"): -3.00

   # ,("inventory level: 1", "order 3 item(s)", "inventory level: 0"): 0
   # ,("inventory level: 1", "order 3 item(s)", "inventory level: 1"): 0
   # ,("inventory level: 1", "order 3 item(s)", "inventory level: 2"): 0
   # ,("inventory level: 1", "order 3 item(s)", "inventory level: 3"): 0



   ,("inventory level: 2", "order 0 item(s)", "inventory level: 0"): +6.00
   ,("inventory level: 2", "order 0 item(s)", "inventory level: 1"): +6.00
   ,("inventory level: 2", "order 0 item(s)", "inventory level: 2"): +6.00
   ,("inventory level: 2", "order 0 item(s)", "inventory level: 3"): +6.00

   ,("inventory level: 2", "order 1 item(s)", "inventory level: 0"): -1.00
   ,("inventory level: 2", "order 1 item(s)", "inventory level: 1"): -1.00
   ,("inventory level: 2", "order 1 item(s)", "inventory level: 2"): -1.00
   ,("inventory level: 2", "order 1 item(s)", "inventory level: 3"): -1.00

   # ,("inventory level: 2", "order 2 item(s)", "inventory level: 0"): 0
   # ,("inventory level: 2", "order 2 item(s)", "inventory level: 1"): 0
   # ,("inventory level: 2", "order 2 item(s)", "inventory level: 2"): 0
   # ,("inventory level: 2", "order 2 item(s)", "inventory level: 3"): 0

   # ,("inventory level: 2", "order 3 item(s)", "inventory level: 0"): 0
   # ,("inventory level: 2", "order 3 item(s)", "inventory level: 1"): 0
   # ,("inventory level: 2", "order 3 item(s)", "inventory level: 2"): 0
   # ,("inventory level: 2", "order 3 item(s)", "inventory level: 3"): 0



   ,("inventory level: 3", "order 0 item(s)", "inventory level: 0"): +5.00
   ,("inventory level: 3", "order 0 item(s)", "inventory level: 1"): +5.00
   ,("inventory level: 3", "order 0 item(s)", "inventory level: 2"): +5.00
   ,("inventory level: 3", "order 0 item(s)", "inventory level: 3"): +5.00

   # ,("inventory level: 3", "order 1 item(s)", "inventory level: 0"): 0
   # ,("inventory level: 3", "order 1 item(s)", "inventory level: 1"): 0
   # ,("inventory level: 3", "order 1 item(s)", "inventory level: 2"): 0
   # ,("inventory level: 3", "order 1 item(s)", "inventory level: 3"): 0

   # ,("inventory level: 3", "order 2 item(s)", "inventory level: 0"): 0
   # ,("inventory level: 3", "order 2 item(s)", "inventory level: 1"): 0
   # ,("inventory level: 3", "order 2 item(s)", "inventory level: 2"): 0
   # ,("inventory level: 3", "order 2 item(s)", "inventory level: 3"): 0

   # ,("inventory level: 3", "order 3 item(s)", "inventory level: 0"): 0
   # ,("inventory level: 3", "order 3 item(s)", "inventory level: 1"): 0
   # ,("inventory level: 3", "order 3 item(s)", "inventory level: 2"): 0
   # ,("inventory level: 3", "order 3 item(s)", "inventory level: 3"): 0
}

inventory_terminal_rewards = {
    "inventory level: 0": 0
   ,"inventory level: 1": 0
   ,"inventory level: 2": 0
   ,"inventory level: 3": 0
}

inventory_mdp = MDP(
          states=inventory_states
         ,actions=inventory_actions
         ,transition_probabilities=inventory_transition_probabilities
         ,rewards=inventory_rewards
         ,terminal_rewards=inventory_terminal_rewards
)

## ---------------------------------------------------------------------------------------------

if __name__ == "__main__":
    epochs = 10
    result = value_iteration(epochs, inventory_mdp,1)
    expected_rewards = result["expected rewards"]
    optimal_policy = result["policy"]

    print '\n'*6
    print "*** Optimal policy has been computed:"
    print ""
    for epoch in xrange(1,epochs):
        i = 1
        for state in inventory_mdp.states:
            if i == 1:
                print "Decision %s: '%s' -> '%s' (expected reward: %s)" % (
                        epoch,
                        state,
                        optimal_policy[(epoch,state)],
                        expected_rewards[(epoch,state)]                        
                        )
            else:
                print "            '%s' -> '%s' (expected reward: %s)" % (
                        state,
                        optimal_policy[(epoch,state)],
                        expected_rewards[(epoch,state)]                        
                        )
            i += 1
        print ""
    print '\n'*2
